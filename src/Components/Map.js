import React, { Component } from 'react'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { pick } from 'lodash'
import Olmap from 'ol/Map.js'
import View from 'ol/View.js'
import { setBaseLayer, setInitial } from '../Action/Actionmap'

class Map extends Component {
  static propTypes = {
    divMap: PropTypes.string.isRequired,
    mapClassName: PropTypes.string,
    divStyle: PropTypes.object,
    defaultZoomControls: PropTypes.bool,
    baseLayer: PropTypes.object.isRequired
  }

  static defaultProps = {
    divMap: 'map',
    mapClassName: '',
    divStyle: {},
    defaultZoomControls: true
  }

  componentDidMount() {
    this.configureMap()
  }

  async configureMap() {
    const map = new Olmap({
      target: this.props.divMap,
      view: new View({
        center: [0, 0],
        zoom: 2
      })
    })
    await this.visibleDefaultControls(map)
    await this.props.setInitial(map)
    await this.props.setBaseLayer(this.props.baseLayer)
  }

  visibleDefaultControls(map) {
    if (!this.props.defaultZoomControls) {
      let listControl = map.getControls().array_
      let zoomControl = listControl.filter(
        control => control.constructor.name === 'Zoom'
      )[0]
      map.removeControl(zoomControl)
    }
    return map
  }

  render() {
    const { divMap, divClassName, divStyle } = this.props
    return (
      <div id={divMap} style={divStyle} className={divClassName}>
        <div style={{ position: 'absolute', zIndex: 1000 }}>
          {this.props.children}
        </div>
      </div>
    )
  }
}

const enhance = connect(
  state => pick(state, ['olmap']),
  { setInitial, setBaseLayer }
)

export default enhance(Map)
