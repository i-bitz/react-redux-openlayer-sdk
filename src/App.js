import React, { Component } from 'react'
// import logo from './logo.svg'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './Examples.css'
import Home from './ExamPages/Home'
import Basicmap from './ExamPages/Basicmap'
import Controlmap from './ExamPages/Controlmap'
import Changebasemap from './ExamPages/Changebasemap'
import Overlayvectortile from './ExamPages/Overlayvectortile'

class App extends Component {
  render() {
    return (
      <Router>
        <Route
          render={({ location }) => (
            <Switch location={location}>
              <Route path="/overlayvectortile" component={Overlayvectortile} />
              <Route path="/changebasemap" component={Changebasemap} />
              <Route path="/controlmap" component={Controlmap} />
              <Route path="/basicmap" component={Basicmap} />
              <Route exact path="/" component={Home} />
            </Switch>
          )}
        />
      </Router>
    )
  }
}

export default App
