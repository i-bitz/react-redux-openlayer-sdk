import { MAP } from '../action-types'

function addLayer(state, action) {
  const { map, layers } = state
  const { layer } = action

  map.addLayer(layer)
  return {
    ...state,
    map,
    layers: [...layers, ...layer.ol_uid],
    activeLayer: layer.ol_uid
  }
}

function setBaseLayer(state, action) {
  const { map } = state
  if (map.getLayers().getLength() === 0) {
    map.addLayer(action.layer)
  } else {
    map.getLayers().removeAt(0)
    map.getLayers().insertAt(0, action.layer)
  }
  return { ...state, map }
}

function setInitial(state, action) {
  return { ...state, map: action.map }
}

function zoomIn(state) {
  const { map } = state
  map.getView().animate({ duration: 500, zoom: map.getView().getZoom() + 1 })
  return { ...state, map }
}

function zoomOut(state) {
  const { map } = state
  map.getView().animate({ duration: 500, zoom: map.getView().getZoom() - 1 })
  return { ...state, map }
}

function flyTo(state, action) {
  const { map } = state
  map
    .getView()
    .fit(action.extent, { duration: action.duration, maxZoom: action.maxZoom })
  return { ...state, map }
}

function panTo(state, action) {
  const { map } = state
  map.getView().animate({
    center: action.center,
    zoom: action.zoom,
    duration: action.duration
  })
  return { ...state, map }
}

export const olmap = (
  state = {
    map: null,
    layers: [],
    activeLayer: null
  },
  action
) => {
  switch (action.type) {
    case MAP.SET_INITIAL:
      return setInitial(state, action)
    case MAP.ZOOM_IN:
      return zoomIn(state)
    case MAP.ZOOM_OUT:
      return zoomOut(state)
    case MAP.FLY_TO:
      return flyTo(state, action)
    case MAP.PAN_TO:
      return panTo(state, action)
    case MAP.SET_BASE_LAYER:
      return setBaseLayer(state, action)
    case MAP.ADD_LAYER:
      return addLayer(state, action)
    default:
      return state
  }
}
