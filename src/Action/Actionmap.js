import { MAP } from '../action-types'

export function setInitial(map) {
  return {
    type: MAP.SET_INITIAL,
    map
  }
}

export function zoomIn() {
  return {
    type: MAP.ZOOM_IN
  }
}

export function zoomOut() {
  return {
    type: MAP.ZOOM_OUT
  }
}

export function flyTo(extent, duration, maxZoom) {
  return {
    type: MAP.FLY_TO,
    extent,
    duration,
    maxZoom
  }
}

export function panTo(center, duration) {
  return {
    type: MAP.PAN_TO,
    center,
    duration
  }
}

export function setBaseLayer(layer) {
  return {
    type: MAP.SET_BASE_LAYER,
    layer
  }
}

export function addLayer(layer) {
  return {
    type: MAP.ADD_LAYER,
    layer
  }
}
