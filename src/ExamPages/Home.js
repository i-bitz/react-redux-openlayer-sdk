import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Logo from '../Logo.png'

class Home extends Component {
  render() {
    return (
      <div>
        <img src={Logo} style={{ height: 80 }} />
        <h2>Example for use react-redux-openlayer-sdk</h2>
        <ul>
          <li>
            <Link to="/basicmap">Basic Map</Link>
          </li>
          <li>
            <Link to="/controlmap">Control Map</Link>
          </li>
          <li>
            <Link to="/changebasemap">Change Base Map</Link>
          </li>
          <li>
            <Link to="/overlayvectortile">Overlay vectortile layer</Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default Home
