import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import { OSM } from 'ol/source'
import { fromLonLat } from 'ol/proj.js'
import Map from '../Components/Map'
import HighLight from 'react-highlight-js'
import { zoomIn, zoomOut, flyTo, panTo } from '../Action/Actionmap'

class Controlmap extends Component {
  render() {
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Control map</h2>
        <Map
          baseLayer={baseLayer}
          divStyle={{ height: 500 }}
          defaultZoomControls={false}
        />
        <p>
          <button
            onClick={() => {
              this.props.zoomIn()
            }}
          >
            zoom in
          </button>{' '}
          <button
            onClick={() => {
              this.props.zoomOut()
            }}
          >
            zoom out
          </button>
        </p>
        <p>
          <button
            onClick={() => {
              let extent = [
                ...fromLonLat([100.536315, 13.725831]),
                ...fromLonLat([100.545931, 13.734195])
              ]
              this.props.flyTo(extent, 1000, 18)
            }}
          >
            fly to Lumphini Park
          </button>{' '}
          <button
            onClick={() => {
              this.props.panTo(fromLonLat([100.662603, 13.687046]), 1000)
            }}
          >
            pan to King Rama IX Park
          </button>
        </p>
        <div style={{ backgroundColor: '#faebd7', padding: '0px 30px' }}>
          <HighLight language="javascript">
            {`
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import { OSM } from 'ol/source'
import { fromLonLat } from 'ol/proj.js'
import Map from '../Components/Map'
import { zoomIn, zoomOut, flyTo, panTo } from '../Action/Actionmap'

class Controlmap extends Component {
  render() {
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Control map</h2>
        <Map
          baseLayer={baseLayer}
          divStyle={{ height: 500 }}
          defaultZoomControls={false}
        />
        <p>
          <button
            onClick={() => {
              this.props.zoomIn()
            }}
          >
            zoom in
          </button>{' '}
          <button
            onClick={() => {
              this.props.zoomOut()
            }}
          >
            zoom out
          </button>
        </p>
        <p>
          <button
            onClick={() => {
              let extent = [
                ...fromLonLat([100.536315, 13.725831]),
                ...fromLonLat([100.545931, 13.734195])
              ]
              this.props.flyTo(extent, 1000, 18)
            }}
          >
            fly to Lumphini Park
          </button>{' '}
          <button
            onClick={() => {
              this.props.panTo(fromLonLat([100.662603, 13.687046]), 1000)
            }}
          >
            pan to King Rama IX Park
          </button>
        </p>
        
      </div>
    )
  }
}
const enhance = connect(
  state => pick(state, ['olmap']),
  { zoomIn, zoomOut, flyTo, panTo }
)

export default enhance(Controlmap)

`}
          </HighLight>
        </div>
      </div>
    )
  }
}

const enhance = connect(
  state => pick(state, ['olmap']),
  { zoomIn, zoomOut, flyTo, panTo }
)

export default enhance(Controlmap)
