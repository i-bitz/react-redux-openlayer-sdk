import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import XYZSource from 'ol/source/XYZ'
import { OSM } from 'ol/source'
import VectorTileLayer from 'ol/layer/VectorTile'
import VectorTileSource from 'ol/source/VectorTile'
import MVT from 'ol/format/MVT'
import Map from '../Components/Map'
import HighLight from 'react-highlight-js'
import { setBaseLayer } from '../Action/Actionmap'
import { applyStyle } from 'ol-mapbox-style'
import Config from '../Config'
import tomtomStyle from '../Config/Jsonstyle/tomtomStyle'
import vallarisStyle from '../Config/Jsonstyle/vallarisStyle'

class Changebasemap extends Component {
  handleChangeBasemap(e) {
    let basemapLayer
    if (e.target.value === 'osm') {
      basemapLayer = new TileLayer({
        source: new OSM()
      })
    } else if (e.target.value === 'google') {
      basemapLayer = new TileLayer({
        preload: 4, //Infinity,
        source: new XYZSource({
          url: `http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}`,
          attributions: '© google map'
        })
      })
    } else if (e.target.value === 'tomtom') {
      basemapLayer = new VectorTileLayer({
        preload: 4,
        source: new VectorTileSource({
          attributions: '© TOMTOM',
          format: new MVT(),
          tilePixelRatio: 8,
          url: `https://b.api.tomtom.com/map/1/tile/basic/main/{z}/{x}/{y}.pbf?key=${
            Config.tomtomApiKey
          }`,
          maxZoom: 14
        })
      })
      applyStyle(basemapLayer, tomtomStyle, 'vectorTiles')
    } else {
      basemapLayer = new VectorTileLayer({
        preload: 4,
        source: new VectorTileSource({
          attributions: '© i-bitz company',
          format: new MVT(),
          tilePixelRatio: 8,
          url: `https://api.vallaris.space/v2/osm/{z}/{x}/{y}?key=${
            Config.vallarisApiKey
          }`,
          maxZoom: 14
        })
      })
      applyStyle(basemapLayer, vallarisStyle, 'openmaptiles')
    }
    this.props.setBaseLayer(basemapLayer)
  }
  render() {
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Change Base Map</h2>
        <Map baseLayer={baseLayer} divStyle={{ height: 500 }} />
        <br />
        <select
          id="list-basemap"
          onChange={this.handleChangeBasemap.bind(this)}
        >
          <option value="osm">osm (Raster Tile)</option>
          <option value="google">google (Raster Tile)</option>
          <option value="tomtom">TOMTOM (Vector Tile)</option>
          <option value="i-bitz">i-bitz (Vector Tile)</option>
        </select>
        <HighLight language="javascript">
          {`
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import XYZSource from 'ol/source/XYZ'
import { OSM } from 'ol/source'
import VectorTileLayer from 'ol/layer/VectorTile'
import VectorTileSource from 'ol/source/VectorTile'
import MVT from 'ol/format/MVT'
import Map from '../Components/Map'
import { setBaseLayer } from '../Action/Actionmap'
import { applyStyle } from 'ol-mapbox-style'
import Config from '../Config'
import tomtomStyle from '../Config/Jsonstyle/tomtomStyle'
import vallarisStyle from '../Config/Jsonstyle/vallarisStyle'

class Changebasemap extends Component {
  handleChangeBasemap(e) {
    let basemapLayer
    if (e.target.value === 'osm') {
      basemapLayer = new TileLayer({
        source: new OSM()
      })
    } else if (e.target.value === 'google') {
      basemapLayer = new TileLayer({
        preload: 4, //Infinity,
        source: new XYZSource({
          url: 'http://mt0.google.com/vt/lyrs=y&hl=en&x={x}&y={y}&z={z}',
          attributions: '© google map'
        })
      })
    } else if (e.target.value === 'tomtom') {
      basemapLayer = new VectorTileLayer({
        preload: 4,
        source: new VectorTileSource({
          attributions: '© TOMTOM',
          format: new MVT(),
          tilePixelRatio: 8,
          url: 'https://b.api.tomtom.com/map/1/tile/basic/main/{z}/{x}/{y}.pbf?key=' + {
            Config.tomtomApiKey
          }',
          maxZoom: 14
        })
      })
      applyStyle(basemapLayer, tomtomStyle, 'vectorTiles')
    } else {
      basemapLayer = new VectorTileLayer({
        preload: 4,
        source: new VectorTileSource({
          attributions: '© i-bitz company',
          format: new MVT(),
          tilePixelRatio: 8,
          url: 'https://api.vallaris.space/v2/osm/{z}/{x}/{y}?key=' + {
            Config.vallarisApiKey
          }',
          maxZoom: 14
        })
      })
      applyStyle(basemapLayer, vallarisStyle, 'openmaptiles')
    }
    this.props.setBaseLayer(basemapLayer)
  }
  render() {
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Change Base Map</h2>
        <Map baseLayer={baseLayer} divStyle={{ height: 500 }} />
        <br />
        <select
          id="list-basemap"
          onChange={this.handleChangeBasemap.bind(this)}
        >
          <option value="osm">osm (Raster Tile)</option>
          <option value="google">google (Raster Tile)</option>
          <option value="i-bitz">i-bitz (Vector Tile)</option>
        </select>
      </div>
    )
  }
}

const enhance = connect(
  state => pick(state, ['olmap']),
  { setBaseLayer }
)

export default enhance(Changebasemap)
`}
        </HighLight>
      </div>
    )
  }
}

const enhance = connect(
  state => pick(state, ['olmap']),
  { setBaseLayer }
)

export default enhance(Changebasemap)
