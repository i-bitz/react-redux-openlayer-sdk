import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import { OSM } from 'ol/source'
import VectorTileLayer from 'ol/layer/VectorTile'
import VectorTileSource from 'ol/source/VectorTile'
import MVT from 'ol/format/MVT'
import HighLight from 'react-highlight-js'
import Map from '../Components/Map'
import { addLayer } from '../Action/Actionmap'

class Overlayvectortile extends Component {
  handlerAddVtLayer() {
    let vectorTileLayer = new VectorTileLayer({
      preload: 4,
      source: new VectorTileSource({
        format: new MVT(),
        tilePixelRatio: 8,
        url: 'https://vt.foss4g.in.th/data/BasinArea/{z}/{x}/{y}.pbf',
        maxZoom: 14
      }),
      zIndex: 1
    })
    this.props.addLayer(vectorTileLayer)
  }
  render() {
    const { activeLayer } = this.props.olmap
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Overlay vectortile layer</h2>
        <Map baseLayer={baseLayer} divStyle={{ height: 500 }} />
        <div>
          <button
            onClick={this.handlerAddVtLayer.bind(this)}
            disabled={activeLayer ? true : false}
          >
            Add vectortile layer
          </button>
        </div>
        <div style={{ backgroundColor: '#faebd7', padding: '0px 30px' }}>
          <HighLight language="javascript">{`
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import { OSM } from 'ol/source'
import VectorTileLayer from 'ol/layer/VectorTile'
import VectorTileSource from 'ol/source/VectorTile'
import MVT from 'ol/format/MVT'
import Map from '../Components/Map'
import { addLayer } from '../Action/Actionmap'

class Overlayvectortile extends Component {
  handlerAddVtLayer() {
    let vectorTileLayer = new VectorTileLayer({
      preload: 4,
      source: new VectorTileSource({
        attributions: '© i-bitz company',
        format: new MVT(),
        tilePixelRatio: 8,
        url: 'https://vt.foss4g.in.th/data/BasinArea/{z}/{x}/{y}.pbf',
        maxZoom: 14
      }),
      zIndex: 1
    })
    this.props.addLayer(vectorTileLayer)
  }
  render() {
    const { activeLayer } = this.props.olmap
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Basic Map</h2>
        <Map baseLayer={baseLayer} divStyle={{ height: 500 }} />
        <button
            onClick={this.handlerAddVtLayer.bind(this)}
            disabled={activeLayer ? true : false}
          >
            Add vectortile layer
          </button>
      </div>
    )
  }
}
const enhance = connect(
  state => pick(state, ['olmap']),
  { addLayer }
)

export default enhance(Overlayvectortile)
`}</HighLight>
        </div>
      </div>
    )
  }
}
const enhance = connect(
  state => pick(state, ['olmap']),
  { addLayer }
)

export default enhance(Overlayvectortile)
