import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import { OSM } from 'ol/source'
import HighLight from 'react-highlight-js'
import Map from '../Components/Map'

class Basicmap extends Component {
  render() {
    const baseLayer = new TileLayer({
      source: new OSM(),
      zIndex: 0
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Basic Map</h2>
        <Map baseLayer={baseLayer} divStyle={{ height: 500 }} />
        <div style={{ backgroundColor: '#faebd7', padding: '0px 30px' }}>
          <HighLight language="javascript">{`
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { pick } from 'lodash'
import { Tile as TileLayer } from 'ol/layer'
import { OSM } from 'ol/source'
import Map from '../Components/Map'

class Basicmap extends Component {
  render() {
    const baseLayer = new TileLayer({
      source: new OSM()
    })
    return (
      <div>
        <Link to="/">
          <button>back to menu</button>
        </Link>
        <h2>Basic Map</h2>
        <Map baseLayer={baseLayer} divStyle={{ height: 500 }} />
      </div>
    )
  }
}
const enhance = connect(
  state => pick(state, ['olmap']),
  {}
)

export default enhance(Basicmap)
`}</HighLight>
        </div>
      </div>
    )
  }
}
const enhance = connect(
  state => pick(state, ['olmap']),
  {}
)

export default enhance(Basicmap)
