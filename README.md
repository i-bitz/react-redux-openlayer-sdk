# react-redux-openlayer-sdk

<img src="https://gitlab.com/i-bitz/react-redux-openlayer-sdk/raw/master/src/Logo.png" width="100">

Javascript SDK based on React, OpenLayers and Redux.

## Using the SDK source to develop a local project

To run the examples,

First, clone the project: `git clone https://gitlab.com/i-bitz/react-redux-openlayer-sdk.git`

Then change into directory of cloned project: `cd react-redux-openlayer-sdk`

Add your API Key in `src/Config/index.js` in there:

```
tomtomApiKey: <your key here>,
mapboxApiKey: <your key here>,
vallarisApiKey: <your key here>
```

After this, run the following commands:

`npm install` - install libraries

`npm start` - start webpack-dev-server

The last command should open your browser to a page of examples: http://localhost:3000